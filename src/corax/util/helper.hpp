#ifndef CORAX_UTIL_HELPER_H_
#define CORAX_UTIL_HELPER_H_

#include <string>
#include <memory>
#include <vector>
#include <corax/model/evolmodel.hpp>
#include <corax/tree/utree.h>
#include <corax/tree/treeinfo.h>
#include <corax/core/common.h>


namespace corax {
namespace util {

struct corax_sequence {
  corax_sequence(char *label, char *seq, unsigned int len);
  ~corax_sequence();
  char *label;
  char *seq;
  unsigned int len;
};

using corax_sequence_ptr = std::unique_ptr<corax_sequence>;
using corax_alignment = std::vector<corax_sequence_ptr>;

struct corax_simple_instance {
  corax_utree_t *utree;
  corax_partition_t *partition;
  corax_treeinfo_t *treeinfo;
};

/**
 *  Initialize a corax_simple_instance (tree, partition, treeinfo)
 *  from a tree file, an alignment file and a model string (e.g. GTR+G)
 *  Return true in case of success
 */ 
bool corax_create_simple_instance(const std::string &treeFile,
    const std::string &alignmentFile,
    const std::string &modelString,
    corax_simple_instance &instance);


/**
 *  Fill alignment and weights from the input alignment file
 *  alignmentFile (can be fasta or phylip) and the state map
 *  stateMap
 *  Throw an exception when the file cannot be read
 */
void parseMSA(const std::string &alignmentFile, 
    const corax_state_t *stateMap,
    corax_alignment &alignment,
    unsigned int *&weights);

/**
 *  Read a tree from either a newick string or a newick file
 *  Throw an exception when an error occures
 */
corax_utree_t *readNewick(const std::string &str, bool isFile);

/**
 *  Create a corax_treeinfo_t structure from a model, a partition and
 *  a tree
 */
corax_treeinfo_t *buildTreeInfo(const corax::model::EvolModel &model,
    corax_partition_t *partition,
    corax_utree_t *utree);

} // namespace util
} // namespace corax

#endif
