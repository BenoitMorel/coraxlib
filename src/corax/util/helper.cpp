#include <corax/util/helper.hpp>
#include <corax/util/hardware.h>
#include <corax/model/evolmodel.hpp>
#include <iostream>
#include <fstream>
#include <corax/corax.h>
#include <map>
namespace corax {
namespace util {


class corax_helper_exception: public std::exception {
public:
  corax_helper_exception(const std::string &s): msg_(s) {
    msg_ += std::string(corax_errmsg);
  }
  corax_helper_exception(const std::string &s1, 
      const std::string s2): msg_(s1 + s2) {
    msg_ += std::string(corax_errmsg);
  }
  virtual const char* what() const noexcept { return msg_.c_str(); }
  void append(const std::string &str) {msg_ += str;}
  virtual ~corax_helper_exception() {}
private:
  std::string msg_;
};

corax_sequence::corax_sequence(char *label, char *seq, unsigned int len):
  label(label),
  seq(seq),
  len(len) {}
  
corax_sequence::~corax_sequence() 
{
  free(label);
  free(seq);
}

static void parseFasta(const std::string &fastaFile, 
    const corax_state_t *stateMap,
    corax_alignment &sequences,
    unsigned int *&weights)
{
  auto reader = corax_fasta_open(fastaFile.c_str(), corax_map_fasta);
  if (!reader) {
    throw corax_helper_exception("Cannot parse fasta file ", fastaFile);
  }
  char * head;
  long head_len;
  char *seq;
  long seq_len;
  long seqno;
  int length;
  while (corax_fasta_getnext(reader, &head, &head_len, &seq, &seq_len, &seqno)) {
    sequences.push_back(corax_sequence_ptr(
          new corax_sequence(head, 
            seq, 
            static_cast<unsigned int>(seq_len))));
    length = static_cast<int>(seq_len);
  }
  unsigned int count = static_cast<unsigned int>(sequences.size());
  char** buffer = static_cast<char**>(malloc(static_cast<size_t>(count) * sizeof(char *)));
  assert(buffer);
  for (unsigned int i = 0; i < count; ++i) {
    buffer[i] = sequences[i]->seq;
  }
  weights = corax_compress_site_patterns(buffer, stateMap, static_cast<int>(count), &length);
  if (!weights) {
    throw corax_helper_exception("Error while parsing fasta: cannot compress sites from ", fastaFile); ;
    free(buffer);
    corax_fasta_close(reader);
  }
  for (unsigned int i = 0; i < count; ++i) {
    sequences[i]->len = static_cast<unsigned int>(length);
  }
  free(buffer);
  corax_fasta_close(reader);
}

static void parsePhylip(const std::string &phylipFile, 
    const corax_state_t *stateMap,
    corax_alignment &sequences,
    unsigned int *&weights)
{
  assert(stateMap);
  std::unique_ptr<corax_phylip_t, void (*)(corax_phylip_t*)> reader(corax_phylip_open(phylipFile.c_str(), corax_map_phylip),
      corax_phylip_close);
  if (!reader) {
    throw corax_helper_exception("Error while opening phylip file ", phylipFile);
  }
  corax_msa_t *msa = nullptr;
  try {
    msa = corax_phylip_parse_interleaved(reader.get());
    if (!msa) {
      throw corax_helper_exception("failed to parse ", phylipFile);
    }
  } catch (...) {
    std::unique_ptr<corax_phylip_t, void(*)(corax_phylip_t*)> 
      reader2(corax_phylip_open(phylipFile.c_str(), corax_map_phylip), corax_phylip_close);
    msa = corax_phylip_parse_sequential(reader2.get());
    if (!msa) {
      throw corax_helper_exception("failed to parse ", phylipFile);
    }
  }
  weights = corax_compress_site_patterns(msa->sequence, stateMap, msa->count, &msa->length);
  if (!weights) 
    throw corax_helper_exception("Error while parsing fasta: cannot compress sites");
  for (auto i = 0; i < msa->count; ++i) {
    corax_sequence_ptr seq(new corax_sequence(msa->label[i], 
          msa->sequence[i], 
          static_cast<unsigned int>(msa->length)));
    sequences.push_back(std::move(seq));
    // don't free these buffers with corax_msa_destroy
    msa->label[i] = nullptr;
    msa->sequence[i] = nullptr;
  }
  corax_msa_destroy(msa);
}
  


void parseMSA(const std::string &alignmentFile, 
    const corax_state_t *stateMap,
    corax_alignment &alignment,
    unsigned int *&weights)
{
  if (!std::ifstream(alignmentFile).good()) {
    throw corax_helper_exception("Alignment file " + 
        alignmentFile + "does not exist");
  }
  try {
    parseFasta(alignmentFile.c_str(),
        stateMap, alignment, weights);
  } catch (...) {
    parsePhylip(alignmentFile.c_str(),
        stateMap, alignment,
        weights);
  }
}
  
static unsigned int getBestAttribute() {
  corax_hardware_probe();
  unsigned int arch = CORAX_ATTRIB_ARCH_CPU;
  if (corax_hardware.avx2_present) {
    arch = CORAX_ATTRIB_ARCH_AVX2;
  } else if (corax_hardware.avx_present) {
    arch = CORAX_ATTRIB_ARCH_AVX;
  } else if (corax_hardware.sse_present) {
    arch = CORAX_ATTRIB_ARCH_SSE;
  }
  arch |= CORAX_ATTRIB_SITE_REPEATS;
  return  arch;
}
static corax_utree_t *readNewickFromStr(const std::string &str) 
{
  auto utree =  corax_utree_parse_newick_rooted(str.c_str());
  if (!utree) { 
    throw corax_helper_exception(
        "Error while reading tree from std::string: ", str);
  }
  return utree;
}


static corax_utree_t *readNewickFromFile(const std::string &str)
{
  try {
    auto utree =  corax_utree_parse_newick_rooted(str.c_str());
    if (!utree) {
      throw corax_helper_exception(
          "Error while reading tree from file: ", str);
    }
    return utree;
  } catch (...) {
      throw corax_helper_exception(
          "Error while reading tree from file: ", str);
  }
}

corax_utree_t *readNewick(const std::string &str, bool isFile)
{
  if (isFile) {
    return readNewickFromFile(str);
  } else {
    return readNewickFromStr(str);
  }
}

static corax_partition_t * buildPartition(corax_utree_t &utree,
  const corax_alignment &alignment, 
  unsigned int *patternWeights,
  const corax::model::EvolModel &model,
  unsigned int attribute)  
{  
  unsigned int tipNumber = static_cast<unsigned int>(alignment.size());
  unsigned int innerNumber = tipNumber -1;
  unsigned int edgesNumber = 2 * tipNumber - 1;
  unsigned int sitesNumber = alignment[0]->len;
  unsigned int ratesMatrices = 1;
  corax_partition_t *partition = corax_partition_create(tipNumber,
      innerNumber,
      model.num_states(),
      sitesNumber, 
      ratesMatrices, 
      edgesNumber,// prob_matrices
      model.num_ratecats(),  
      edgesNumber,// scalers
      attribute);  
  corax_set_pattern_weights(partition, patternWeights);
  
  // fill partition
  std::map<std::string, unsigned int> tipsLabelling;
  unsigned int labelIndex = 0;
  for (auto &seq: alignment) {
    tipsLabelling[seq->label] = labelIndex;
    corax_set_tip_states(partition, labelIndex, model.charmap(), seq->seq);
    labelIndex++;
  }
  assign(partition, model);
  
  // map tree to partition
  for (unsigned int i = 0; i < utree.tip_count; ++i) {
    auto leaf = utree.nodes[i];
    assert(tipsLabelling.find(leaf->label) != tipsLabelling.end());
    leaf->clv_index = tipsLabelling[leaf->label];
  }
  return partition;
}

corax_treeinfo_t *buildTreeInfo(const corax::model::EvolModel &model,
    corax_partition_t *partition,
    corax_utree_t *utree)
{
  // treeinfo
  int params_to_optimize = model.params_to_optimize();
  params_to_optimize |= CORAX_OPT_PARAM_BRANCHES_ITERATIVE;
  std::vector<unsigned int> params_indices(model.num_ratecats(), 0);
  auto anyInnerNode = utree->nodes[utree->tip_count];
  auto treeinfo = corax_treeinfo_create(anyInnerNode, 
      utree->tip_count, 1, CORAX_BRLEN_SCALED);
  corax_treeinfo_init_partition(treeinfo, 0, partition,
      params_to_optimize,
      model.gamma_mode(),
      model.alpha(), 
      &params_indices[0],
      model.submodel(0).rate_sym().data());
  return treeinfo;
}

bool corax_create_simple_instance(const std::string &treeFile,
    const std::string &alignmentFile,
    const std::string &modelString,
    corax_simple_instance &instance)
{
  corax::model::EvolModel model(modelString);
  auto attribute = getBestAttribute();
  corax_alignment alignment;
  unsigned int *patternWeights = nullptr;
  parseMSA(alignmentFile, 
      model.charmap(), 
      alignment, 
      patternWeights);
  instance.utree = readNewick(treeFile, true);  
  instance.partition = buildPartition(*instance.utree,
      alignment, 
      patternWeights, 
      model,
      attribute);
  instance.treeinfo = buildTreeInfo(model, 
      instance.partition, 
      instance.utree);   
  free(patternWeights);
  return true;
}


} // namespace util
} // namespace corax

